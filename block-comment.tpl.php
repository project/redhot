<div class="sidebox">
	<div class="boxhead">
  <?php if($block->subject) { ?>
    <h2 class="title"><?php print $block->subject; ?></h2>
	<?php } ?>
  </div>
	<div class="boxbody">
		<?php print $block->content; ?>
	</div>
</div>
