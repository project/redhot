<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php if ($right);else echo "<style type=\"text/css\" media=\"all\">#main {width:650px;}  #main .inner {padding-right:0px;} #mission {margin-right:0;}</style>"; ?>
  <?php if ($left);else echo "<style type=\"text/css\" media=\"all\">#main {width:650px;} #main .inner {padding-left:0px;} #mission {margin-left:0;}</style>"; ?>
  <?php if ($right);elseif($left);else echo "<style type=\"text/css\" media=\"all\">#main {width:800px;} #main .inner {padding:0px;} #mission {margin:0;}</style>"; ?>
  <!--[if IE]>
  <style>#search {position:relative;top:-15px;}</style>
  <![endif]-->
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">
  <div id="wrapper">
      <!--  <?php if (isset($secondary_links)) { ?><div id="menu"><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?></div><?php } ?> -->
      <div id="header-region">
      <?php print $header ?>
      </div>
    <div id="logo"><?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    <?php if ($search_box) : ?><div class="search-box"><?php print $search_box ?></div><?php endif; ?></div>
  <br clear="all"/>
  <div id="header" class="clear-block">
    <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
    <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </div>
    <!-- <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?></div><?php } ?> -->
    <?php if ($left) { ?>
      <div id="sidebar-left" class="column">
      <?php print $left ?>
      </div>
    <?php } ?>
    <div id="main" class="column">
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div class="inner">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <?php if ($tabs){ ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
        <?php print $help ?>
        <?php if ($show_messages): print $messages; endif; ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>
    </div>
    <?php if ($right): ?>
      <div id="sidebar-right" class="column"><div id="sr2"><div id="sr3"><div id="sr4"><div id="sr5">
      <?php print $right ?>
      </div></div></div></div></div>
    <?php endif; ?>
  </div>
  <br clear="all"/>
  <div id="footer">
  <?php echo $footer_message . $footer;?><br/>

  </div>
  <?php print $closure ?>
  </div>
</body>
</html>
