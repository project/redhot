CONTENTS

1. Installation
2. Activation
3. Contact
4. Credits

********************************************************************************************
1. INSTALLATION 

To install this theme unpack RedHot folder in your drupal installation
themes directory, located at your_drupal_site/themes .  

2. ACTIVATION
Then log into your drupal site with an account that has the level of permissions to activate themes:
 
Then:
 
Administer->Site Building-> Themes

If you installed the theme correctly you should RedHot Theme listed amongst a list of others. 
Tick the "Enabled" check box next to RedHot to enable it and if  you wish to make it your default theme,
select the "Default" radio box in line  with RedHot as well.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

3. Contact

Contact: David@davidstclair.co.uk


4. Credits.
 
I looked at lots of other Themes such as forest floor and foundation as well as Garland. I also used a method 
for creating rounded corners on the net.
